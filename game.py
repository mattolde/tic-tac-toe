import random

PLAY_X = 'X'
PLAY_O = "O"
PLAY_EMPTY = " "


def display_board(plays):
    """ Display playing board """

    print(' ----------- ')
    print('|   |   |   |')
    print('| {} | {} | {} |'.format(plays[0], plays[1], plays[2]))
    print('|   |   |   |')
    print('|-----------')
    print('|   |   |   |')
    print('| {} | {} | {} |'.format(plays[3], plays[4], plays[5]))
    print('|   |   |   |')
    print('|-----------')
    print('|   |   |   |')
    print('| {} | {} | {} |'.format(plays[6], plays[7], plays[8]))
    print('|   |   |   |')
    print(' -----------')


def set_player_piece(player):
    """Set player pieces.

    Args:
        player: Type of player either computer or player.

    Returns:
        Return a set of players pieces.
    """

    if player == 'player':

        piece = raw_input(
            'Choose your playing piece {} or {}: '
            .format(PLAY_X, PLAY_O)).upper()

        while piece not in (PLAY_X, PLAY_O):
            piece = raw_input(
                'Please choose {} or {}: '.format(PLAY_X, PLAY_O)).upper()
    else:
        if random.randint(0, 1) == 1:
            piece = PLAY_X
        else:
            piece = PLAY_O

    if piece == PLAY_X:
        return (PLAY_X, PLAY_O)
    else:
        return (PLAY_O, PLAY_X)


def is_board_full(moves):
    """Check if all moves are taken on the boards.

    Args:
        moves: Boards moves list.

    Returns:
        True if board is full. False if not.
    """
    if PLAY_EMPTY in moves:
        return False
    else:
        print('Tie')
        display_board(moves)
        return True


def player_turn(moves, piece):
    """Takes human players move.

    Args:
        moves: Boards moves list.
        piece: player piece

    Returns:
        List of moves. False if invalid input.
    """

    turn = raw_input('Enter in the range of 1-9: ')

    # TODO : to much code in the try statement. Make it clear that it is
    # converting the value to an int.
    try:
        if int(turn) in range(1, 10):
            turn = int(turn) - 1

            # Check if move is already taken. If it is call the function again.
            if moves[turn] not in (PLAY_X, PLAY_O):
                moves[turn] = piece
                return moves
            else:
                print('Choosen move already taken. Choose again.')
        else:
            print('Please keep in range 1 - 9.')

    except:
        print('Input must be numeric in range 1 - 9.')

    return player_turn(moves, piece)


def check_win_move(piece, opponent_piece):
    """Check if a play can be made to win the game

    Args:
        Players pieces

    Returns:
        Move to win. False if no move available
    """

    # winning sets
    winning_lines = [(0, 1, 2), (3, 4, 5), (0, 3, 6), (0, 4, 8), (1, 4, 7),
                     (2, 4, 6), (6, 7, 8), (2, 5, 8), (1, 4, 7)]

    for line in winning_lines:
        count = 0
        opponent_count = 0
        future_move = False

        for move in line:

            if moves[move] == piece:
                count += 1
            elif moves[move] == opponent_piece:
                opponent_count += 1
            else:
                future_move = move

        if count == 2 and future_move is not False:
            return future_move

        if opponent_count == 2 and future_move is not False:
            return future_move

    return False


def computer_turn(moves, computer_piece):
    """Computer takes a turn.

    Args:
        moves: A list of board plays.
        computer_piece: Computer piece.

    Returns:
        A list of board moves. False if unable to make move.
    """

    if computer_piece == PLAY_X:
        player_piece = PLAY_O
    else:
        player_piece = PLAY_X

    win_move = check_win_move(computer_piece, player_piece)

    if win_move is not False and moves[win_move] != player_piece:
        moves[win_move] = computer_piece
        return moves

    # check if player can win
    stop_win_move = check_win_move(player_piece, computer_piece)

    if stop_win_move is not False and moves[stop_win_move] != player_piece:
        moves[stop_win_move] = computer_piece
        return moves

    # corner move
    if moves[0] == PLAY_EMPTY or moves[2] == PLAY_EMPTY or \
       moves[6] == PLAY_EMPTY or moves[8] == PLAY_EMPTY:

        position = random.choice([0, 2, 6, 8])

        if moves[position] == PLAY_EMPTY:
            moves[position] = computer_piece
            return moves

    elif moves[4] == PLAY_EMPTY:
        # take center
        moves[4] = computer_piece
        return moves
    else:
        # take side
        position = random.choice([1, 7, 3, 5])

        if moves[position] == PLAY_EMPTY:
            moves[position] = computer_piece
            return moves

    return computer_turn(moves, computer_piece)


def check_win(moves, piece):
    """ Check if player has won """

    if moves[0] == piece and moves[1] == piece and moves[2] == piece or \
       moves[3] == piece and moves[4] == piece and moves[5] == piece or \
       moves[6] == piece and moves[7] == piece and moves[8] == piece or \
       moves[0] == piece and moves[3] == piece and moves[6] == piece or \
       moves[1] == piece and moves[4] == piece and moves[7] == piece or \
       moves[2] == piece and moves[5] == piece and moves[8] == piece or \
       moves[0] == piece and moves[4] == piece and moves[8] == piece or \
       moves[2] == piece and moves[4] == piece and moves[6] == piece:

        return True
    else:
        return False


if __name__ == "__main__":

    moves = [PLAY_EMPTY] * 9

    # Start game
    raw_input('Flip to see who goes first: (press enter/return)')

    print('Use 1 - 9 to place your moves')

    # Show move locations on board.
    display_board([1, 2, 3, 4, 5, 6, 7, 8, 9])

    if random.randint(0, 1) == 1:
        print('Computer goes first')
        play = 'computer'
        player, computer = set_player_piece('computer')
    else:
        print('You go first')
        play = 'player'
        player, computer = set_player_piece('player')

    # Start playing
    while not is_board_full(moves):

        if play == 'player':
            # Player takes turn

            display_board(moves)

            moves = player_turn(moves, player)

            if check_win(moves, player):
                print('You win!')
                display_board(moves)
                break

            play = 'computer'
        else:
            # Computer takes turn

            moves = computer_turn(moves, computer)

            if check_win(moves, computer):
                print('You lose!')
                display_board(moves)
                break

            play = 'player'
